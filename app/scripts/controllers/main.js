'use strict';

angular.module('naumiFrontApp')
  .controller('MainCtrl', function ($scope,$location, sessionService) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
    $scope.searchCriteria=null;


    $scope.gotoResults=function() {
    	console.log($scope.searchCriteria)
    	sessionService.setSearchCriteria($scope.searchCriteria);
    	$location.path('/results')
    }
  });
