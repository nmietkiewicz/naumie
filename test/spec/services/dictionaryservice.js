'use strict';

describe('Service: dictionaryService', function () {

  // load the service's module
  beforeEach(module('naumiFrontApp'));

  // instantiate service
  var dictionaryService;
  beforeEach(inject(function (_dictionaryService_) {
    dictionaryService = _dictionaryService_;
  }));

  it('should do something', function () {
    expect(!!dictionaryService).toBe(true);
  });

});
