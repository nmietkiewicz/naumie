'use strict';

/**
 * @ngdoc service
 * @name naumiFrontApp.searchService
 * @description
 * # searchService
 * Service in the naumiFrontApp.
 */
angular.module('naumiFrontApp')
    .service('searchService', function() {

        var searchCriteria = {}

        function setSearchCriteria(data) {
            searchCriteria = data;
        }

        function getSearchCriteria() {
            return searchCriteria;
        }

        return {
            setSearchCriteria: setSearchCriteria,
            getSearchCriteria: getSearchCriteria
        }



    });
