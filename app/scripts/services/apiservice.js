'use strict';

/**
 * @ngdoc service
 * @name naumiFrontApp.apiService
 * @description
 * # apiService
 * Service in the naumiFrontApp.
 */
angular.module('naumiFrontApp')
  .service('apiService', function () {
        var tutorList = [{
            name: "Roman Kołtoń",
            date: "dzisiaj",
            time: "12:00",
            place: "Stegny Warszawa",
            price: 41,
            studentCount: 4
        }, {
            name: "Roman Kołtoń",
            date: "dzisiaj",
            time: "12:00",
            place: "Stegny Warszawa",
            price: 41,
            studentCount: 4
        }, {
            name: "Roman Kołtoń",
            date: "dzisiaj",
            time: "12:00",
            place: "Stegny Warszawa",
            price: 41,
            studentCount: 4
        }, {
            name: "Roman Kołtoń",
            date: "dzisiaj",
            time: "12:00",
            place: "Stegny Warszawa",
            price: 41,
            studentCount: 4
        }, {
            name: "Roman Kołtoń",
            date: "dzisiaj",
            time: "12:00",
            place: "Stegny Warszawa",
            price: 41,
            studentCount: 4
        }, {
            name: "Roman Kołtoń",
            date: "dzisiaj",
            time: "12:00",
            place: "Stegny Warszawa",
            price: 41,
            studentCount: 4
        }, {
            name: "Roman Kołtoń",
            date: "dzisiaj",
            time: "12:00",
            place: "Stegny Warszawa",
            price: 41,
            studentCount: 4
        }, {
            name: "Roman Kołtoń",
            date: "dzisiaj",
            time: "12:00",
            place: "Stegny Warszawa",
            price: 41,
            studentCount: 4
        }];


        function getTutors() {
            return tutorList;
        }

        return {
            getTutors: getTutors
        }

  });
