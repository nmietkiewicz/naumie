'use strict';

/**
 * @ngdoc function
 * @name naumiFrontApp.controller:ResultsCtrl
 * @description
 * # ResultsCtrl
 * Controller of the naumiFrontApp
 */
angular.module('naumiFrontApp')
  .controller('ResultsCtrl', function ($scope,sessionService,apiService) {
   
  		$scope.searchCriteria=sessionService.searchCriteria;

  		$scope.tutorList=apiService.getTutors($scope.searchCriteria);
  	

  });
