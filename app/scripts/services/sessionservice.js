'use strict';

/**
 * @ngdoc service
 * @name naumiFrontApp.sessionService
 * @description
 * # sessionService
 * Factory in the naumiFrontApp.
 */
angular.module('naumiFrontApp')
    .factory('sessionService', function() {
        var Session = {
            searchCriteria: {},
            saveSession: function() {},
            setSearchCriteria: function(data) {
                Session.searchCriteria = data
            }
        };

        return Session;
    });
