'use strict';

/**
 * @ngdoc service
 * @name naumiFrontApp.dictionaryService
 * @description
 * # dictionaryService
 * Service in the naumiFrontApp.
 */
angular.module('naumiFrontApp')
    .service('dictionaryService', function() {


        function getSubjectDictionary() {
            return ["Matematyka", "Angielski", "Niemiecki"];
        }

        function getTopicDictionary() {
            return ["trygonometria", "idiomy"];
        }

        return {
            getSubjectDictionary: getSubjectDictionary,
            getTopicDictionary: getTopicDictionary
        }
    });
